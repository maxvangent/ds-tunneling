mod scanner;
mod socket;

use clap::{Parser, Subcommand};
use macaddr::MacAddr6;
use pcap::{Capture, Device};
use std::{error::Error, io, net::SocketAddr, process::Command};

use crate::socket::{IWMode, IWSocket};

pub type Result<T> = ::std::result::Result<T, Box<dyn Error>>;

#[derive(Subcommand, Clone)]
enum ApplicationMode {
    Server {
        #[arg(short, long)]
        device: Option<String>,
        #[arg(short, long)]
        macaddr: Option<MacAddr6>,
    },
    Client { 
        address: SocketAddr 
    }
}

#[derive(Parser)]
struct Args {
    #[command(subcommand)]
    pub application_mode: ApplicationMode,
}

fn main() -> Result<()> {
    let args = Args::parse();

    match args.application_mode {
        ApplicationMode::Server { device, macaddr } => run_server(device, macaddr)?,
        ApplicationMode::Client { address } => todo!(),
    }

    // let mut capture = Capture::from_device(device.clone())?
    //     .rfmon(true)
    //     .promisc(true)
    //     .open()?;

    // println!("{:?}", capture.next_packet()?);

    Ok(())
}

fn run_server(device: Option<String>, macaddr: Option<MacAddr6>,) -> Result<()> {
    let mut devices = Device::list().unwrap();
    let device: Device = match device {
        Some(n) => devices
            .into_iter()
            .find(|d| d.name == n)
            .expect(&format!("Device {} not found", n)),

        None => {
            for (i, d) in devices.iter().enumerate() {
                println!("{}: {}", i, d.name)
            }

            let num: usize = io::stdin()
                .lines()
                .next()
                .expect("Could not read input")?
                .trim()
                .parse()?;
            devices.swap_remove(num)
        }
    };
    let socket = IWSocket::open().expect("Could not open network socket");
    assert!(Command::new("networkctl")
        .args(["down", &device.name])
        .spawn()?
        .wait()?
        .success());
    socket::set_mode(&socket, &device.name, IWMode::Monitor).expect("Could not set device mode");
    assert!(Command::new("networkctl")
        .args(["up", &device.name])
        .spawn()?
        .wait()?
        .success());
    let dss = scanner::scan_for_ds(&socket, &device, macaddr)?;
    let mut dss = Vec::from_iter(dss.iter());

    if dss.is_empty() {
        panic!("No DS's found. Are you hosting a game?");
    }

    let ds = match macaddr {
        Some(mac) => dss
            .into_iter()
            .next()
            .expect(&format!("MAC address {} not found", mac)),

        None => {
            for (i, ds) in dss.iter().enumerate() {
                println!("{}: {} on channel {}", i, ds.macaddr, ds.channel);
            }

            let num: usize = io::stdin()
                .lines()
                .next()
                .expect("Could not read input")?
                .trim()
                .parse()?;
            dss.swap_remove(num)
        }
    };
    Ok(())
}
