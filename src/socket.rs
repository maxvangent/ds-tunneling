use nix::{errno::Errno, ioctl_write_ptr_bad};

pub struct IWSocket(libc::c_int);

impl IWSocket {
    pub fn open() -> Option<Self> {
        const FAMILIES: [libc::c_int; 4] = [
            libc::AF_INET,
            libc::AF_IPX,
            libc::AF_AX25,
            libc::AF_APPLETALK,
        ];

        for family in FAMILIES {
            unsafe {
                let sock = libc::socket(family, libc::SOCK_DGRAM, 0);
                if sock >= 0 {
                    return Some(Self(sock));
                }
            }
        }

        None
    }
}

impl Drop for IWSocket {
    fn drop(&mut self) {
        unsafe { libc::close(self.0) };
    }
}

#[repr(C)]
pub struct SIWMode {
    name: [u8; libc::IFNAMSIZ],
    mode: IWMode,
}

#[repr(C)]
pub struct SIWFreq {
    name: [u8; libc::IFNAMSIZ],
    freq: IWFreq,
}

#[repr(C)]
pub struct IWFreq {
    m: u32,
    e: u16,
    i: u8,
    flags: IWFreqFlags,
}

#[repr(u8)]
pub enum IWFreqFlags {
    FIXED = 1,
}

#[repr(u32)]
pub enum IWMode {
    Monitor = 6,
}

const SIOCSIWFREQ: u16 = 0x8B04;
const SIOCSIWMODE: u16 = 0x8B06;
ioctl_write_ptr_bad!(iw_set_freq, SIOCSIWFREQ, SIWFreq);
ioctl_write_ptr_bad!(iw_set_mode, SIOCSIWMODE, SIWMode);

pub fn set_mode(socket: &IWSocket, device: &str, mode: IWMode) -> Result<(), Errno> {
    let device = device.as_bytes();

    let mut name = [0; libc::IFNAMSIZ];
    name[..device.len()].copy_from_slice(device);

    let data = SIWMode { name, mode };
    unsafe { iw_set_mode(socket.0, &data as *const SIWMode)? };

    Ok(())
}

pub fn set_channel(socket: &IWSocket, device: &str, channel: u32) -> Result<(), Errno> {
    let device = device.as_bytes();

    let mut name = [0; libc::IFNAMSIZ];
    name[..device.len()].copy_from_slice(device);

    let freq = IWFreq {
        m: channel,
        e: 0,
        i: 0, // doesn't matter
        flags: IWFreqFlags::FIXED,
    };

    let data = SIWFreq { name, freq };
    unsafe { iw_set_freq(socket.0, &data as *const SIWFreq)? };

    Ok(())
}
